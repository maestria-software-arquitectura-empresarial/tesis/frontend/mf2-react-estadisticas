# Utilizamos una imagen de node como base
FROM node:latest

# Establecemos el directorio de trabajo en el contenedor
WORKDIR /app

# Copiamos los archivos de la aplicación al directorio de trabajo
COPY ./body2/package.json ./body2/package-lock.json ./

# Instalamos las dependencias del proyecto
RUN npm install

# Copiamos el resto de los archivos de la aplicación al directorio de trabajo
COPY ./body2/ ./

# Exponemos el puerto 9452 (el puerto por defecto de la aplicación React)
EXPOSE 9452

# Comando para iniciar la aplicación cuando se ejecute el contenedor
CMD ["npm", "start"]


# docker build -t mf2_imagen .
# docker run -p 9452:9452 --name mf2_contenedor mf2_imagen
# mf1-react-carga
# body1 npm run build