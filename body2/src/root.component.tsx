import React, { CSSProperties, useState, useEffect, useRef } from 'react';
import validateAndSendRequest from './root.http.services';
import Chart from 'chart.js/auto';

type PositionType = 'static' | 'relative' | 'absolute' | 'sticky' | 'fixed';
type TextAlignType = 'left' | 'right' | 'center' | 'justify' | 'initial' | 'inherit';

interface FormData {
  banco: string;
  fecha: string;
}

const Root = (props: { name: string }) => {
  const [formData, setFormData] = useState<FormData>({ banco: '', fecha: '' });
  const [data, setData] = useState<any>(null);
  const styles: { [key: string]: CSSProperties } = {
    container: {
      fontFamily: 'Poppins, sans-serif',
      maxWidth: '600px',
      margin: '0 auto',
      padding: '20px',
      backgroundColor: '#ffffff',
      color: '#151940',
    },
    column: {
      marginLeft: '1%',
    },
    customDiv: {
      width: '150px',
      height: '234px',
      backgroundColor: '#F5F6FA',
      borderRadius: '10px',
      position: 'relative' as PositionType,
    },
    menu: {
      textAlign: 'center' as TextAlignType,
      marginTop: '20px',
      backgroundColor: '#D1D8F5',
      borderRadius: '10px',
      padding: '10px',
    },
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const { name, value } = e.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleFormSubmit = async () => {
    try {
      const responseData = await validateAndSendRequest(formData);
      setData(responseData);
      alert('Petición enviada con éxito.');
      setFormData({ banco: '', fecha: '' });
    } catch (error) {
      alert('Error al enviar la petición: ' + error.message);
    }
  };

  const chartContainer = useRef(null);


  useEffect(() => {
    if (chartContainer.current && data && data.total_fechas) {
      const chartData = {
        labels: Object.keys(data.total_fechas),
        datasets: [
          {
            label: 'Total por Fechas',
            data: Object.values(data.total_fechas),
            backgroundColor: 'rgba(28, 43, 77, 0.6)',
            borderColor: 'rgba(28, 43, 77, 1)',
            borderWidth: 1,
          },
        ],
      };

      const chartOptions = {
        responsive: true,
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      };

      new Chart(chartContainer.current, {
        type: 'bar',
        data: chartData,
        options: chartOptions,
      });
    }
  }, [data]);

  return (
    <div className="container-fluid">
      <div className="container" style={styles.container}>
        <h3>Estadisticas</h3>
        <h5>Por favor, seleccione criterios de búsquedas.</h5>
        <div className="row">
          <div className="col-12">
            <div className="row">
              <div className="col-6">
                <select
                  name="banco"
                  value={formData.banco}
                  onChange={handleInputChange}
                  style={{ marginBottom: '5%', width: '200px' }}
                >
                  <option value="">Banco de búsqueda</option>
                  <option value="Banco Central del Ecuador">Banco Central del Ecuador</option>
                  <option value="Banco Bolivariano">Banco Bolivariano</option>
                </select>
              </div>
              <div className="col-6">
                <select
                  name="fecha"
                  value={formData.fecha}
                  onChange={handleInputChange}
                  style={{ marginBottom: '5%', width: '200px' }}
                >
                  <option value="">Fecha de búsqueda</option>
                  <option value="abril-2024">Abril 2024</option>
                  <option value="marzo-2024">Marzo 2024</option>
                  <option value="febrero-2024">Febrero 2024</option>
                  <option value="enero-2024">Enero 2024</option>
                </select>
              </div>
            </div>
            <button
              style={{ marginBottom: '5%', width: '500px' }}
              className="btn-success"
              onClick={handleFormSubmit}
            >
              Buscar
            </button>
          </div>
        </div>
        <hr />
        <div className="container">
          <h5>Total de cargas: {data && data.total ? data.total.toString() : ''}</h5>
          <div>
            <h5>Histograma de cargas por fechas</h5>
            <canvas ref={chartContainer} />
          </div>
          <br />
          <h5>Detalle de cargas por fechas</h5>
          <table style={{ border: '1px solid rgb(28, 43, 77)' }}>
            <thead>
              <tr>
                <th style={{ border: '1px solid rgb(28, 43, 77)' }}>ID</th>
                <th style={{ border: '1px solid rgb(28, 43, 77)' }}>Nombre</th>
                <th style={{ border: '1px solid rgb(28, 43, 77)' }}>Referencia</th>
                <th style={{ border: '1px solid rgb(28, 43, 77)' }}>Descripción</th>
                <th style={{ border: '1px solid rgb(28, 43, 77)' }}>Mail</th>
                <th style={{ border: '1px solid rgb(28, 43, 77)' }}>SMS</th>
                <th style={{ border: '1px solid rgb(28, 43, 77)' }}>Banco</th>
                <th style={{ border: '1px solid rgb(28, 43, 77)' }}>Fecha de Carga</th>
              </tr>
            </thead>
            <tbody>
              {data && data.detalle ? (
                data.detalle.map((item) => (
                  <tr key={item.id_carga}>
                    <td style={{ border: '1px solid rgb(28, 43, 77)' }}>{item.id_carga}</td>
                    <td style={{ border: '1px solid rgb(28, 43, 77)' }}>{item.nombre}</td>
                    <td style={{ border: '1px solid rgb(28, 43, 77)' }}>{item.referencia}</td>
                    <td style={{ border: '1px solid rgb(28, 43, 77)' }}>{item.descripcion}</td>
                    <td style={{ border: '1px solid rgb(28, 43, 77)' }}>{item.mail}</td>
                    <td style={{ border: '1px solid rgb(28, 43, 77)' }}>{item.sms}</td>
                    <td style={{ border: '1px solid rgb(28, 43, 77)' }}>{item.banco}</td>
                    <td style={{ border: '1px solid rgb(28, 43, 77)' }}>{item.fechacarga}</td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td colSpan={8}></td>
                </tr>
              )}
            </tbody>
          </table>

        </div>
      </div>
    </div>
  );
};

export default Root;
