import { API_URL } from './config';

interface FormData {
    banco: string;
    fecha: string;
}

const validateAndSendRequest = async (formData: FormData): Promise<void> => {
   
    if (!formData.banco || !formData.fecha) {
        throw new Error('Por favor, seleccione el banco y la fecha.');
    }

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(formData)
    };

    try {
        console.log(JSON.stringify(formData, null, 2));

        const response = await fetch(API_URL, requestOptions);
        if (!response.ok) {
            throw new Error('Error al enviar la petición.');
        }
        const responseData = await response.json();
        console.log('Petición enviada con éxito. \n\n' + JSON.stringify(responseData, null, 2) );
        return responseData;
    } catch (error) {
        console.error('Error en la solicitud REST:', error);
        throw error;
    }
};

export default validateAndSendRequest;
